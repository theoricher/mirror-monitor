# Mirror Monitor

Monitor the status of known mirrors of f-droid.org.



## Active Mirrors

* https://f-droid.org/repo
* rsync://mirror.f-droid.org/
* https://mirror.albony.xyz/fdroid/repo
* https://cloudflare.f-droid.org/repo
* https://fdroid.tetaneutral.net/fdroid/repo
* https://mirror.cyberbits.eu/fdroid/repo
* https://bubu1.eu/fdroid/repo
* https://fdroid.swedneck.xyz/fdroid/repo
* https://ftp.fau.de/fdroid/repo
* http://ftpfaudev4triw2vxiwzf4334e3mynz7osqgtozhbc77fixncqzbyoyd.onion/
* http://fauftpffbmvh3p4h.onion/fdroid/repo
* https://ftp.osuosl.org/pub/fdroid/repo
* https://mirror.scd31.com/fdroid/repo
* https://fdroid.fi-do.io/fdroid/repo
* https://plug-mirror.rcac.purdue.edu/fdroid/repo
* https://mirrors.tuna.tsinghua.edu.cn/fdroid/repo
* https://mirrors.nju.edu.cn/fdroid/repo
* https://mirror.kumi.systems/fdroid/repo
* https://ftp.lysator.liu.se/pub/fdroid/repo
* http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/fdroid/repo
* https://mirror.librelabucm.org/fdroid/repo/
* http://mirror.librelablmozifzc.onion/fdroid/repo/
* https://fdroid-mirror.calyxinstitute.org/fdroid/repo
* https://mirrors.dotsrc.org/fdroid/repo/
* https://mirror.ossplanet.net/fdroid/repo/
* http://mirror.ossplanetnyou5xifr6liw5vhzwc2g2fmmlohza25wwgnnaw65ytfsad.onion/fdroid/repo/
* https://ftp.gwdg.de/pub/android/fdroid/repo/
* https://ftp.snt.utwente.nl/pub/software/fdroid/repo/
* https://fdroid.zw.is/fdroid/repo/
* https://fdroid.astra.in.ua/fdroid/repo
* rsync://fdroid.astra.in.ua/fdroid
* https://ftp.agdsn.de/fdroid/repo/
* https://mirror.cxserv.de/fdroid/repo
* rsync://mirror.cxserv.de/fdroid/repo
* https://mirror.freedif.org/fdroid
* rsync://mirror.freedif.org/fdroid
* https://mirrors.jevincanders.net/fdroid/repo
* rsync://mirrors.jevincanders.net/fdroid/repo
* https://mirror.fcix.net/fdroid/repo/
* rsync://mirror.fcix.net/fdroid/repo/
* https://forksystems.mm.fcix.net/fdroid/repo/
* rsync://forksystems.mm.fcix.net/fdroid/repo/
* https://uvermont.mm.fcix.net/fdroid/repo/
* rsync://uvermont.mm.fcix.net/fdroid/repo/
* https://southfront.mm.fcix.net/fdroid/repo/
* rsync://southfront.mm.fcix.net/fdroid/repo/
* https://ziply.mm.fcix.net/fdroid/repo/
* rsync://ziply.mm.fcix.net/fdroid/repo/
* https://opencolo.mm.fcix.net/fdroid/repo/
* rsync://opencolo.mm.fcix.net/fdroid/repo/
* https://mirror01.komogoto.com/fdroid/repo/
* rsync://mirror01.komogoto.com/fdroid/repo/